package com.gupao.rpc;

import com.gupao.rpc.discovery.IServiceDiscovery;

import java.lang.reflect.Proxy;

public class RpcClientProxy {


    private IServiceDiscovery serviceDiscovery;
    private String version;

    public RpcClientProxy(IServiceDiscovery serviceDiscovery,String version) {
        this.serviceDiscovery = serviceDiscovery;
        this.version = version;
    }

    //创建远程代理类
    public <T> T clientProxy(final Class<?> interfaces){
        return (T) Proxy.newProxyInstance(interfaces.getClassLoader(),new Class[]{interfaces},new RemoteInvocationHander(serviceDiscovery,version));
    }
}
