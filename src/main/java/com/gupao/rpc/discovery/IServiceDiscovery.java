package com.gupao.rpc.discovery;

public interface IServiceDiscovery {

    String discoveryService(String serviceName);
}
