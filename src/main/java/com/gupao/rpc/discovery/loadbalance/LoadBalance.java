package com.gupao.rpc.discovery.loadbalance;

import java.util.List;

public interface LoadBalance {

    String selectHost(List<String> repos);
}
