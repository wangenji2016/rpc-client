package com.gupao.rpc.discovery.loadbalance;

import java.util.List;

public abstract class AbstractLoadBalance implements LoadBalance {

    @Override
    public String selectHost(List<String> repos) {
        if(repos == null || repos.size()==0){
            return null;
        }
        if(repos.size()==1){
            return repos.get(0);
        }else {
            return doSelect(repos);
        }
    }

    public abstract String doSelect(List<String> repos);

}
