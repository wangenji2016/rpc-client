package com.gupao.rpc.discovery.loadbalance;

import java.util.List;
import java.util.Random;

public class RandomLoadBalance extends AbstractLoadBalance{

    @Override
    public String doSelect(List<String> repos) {
        Random random = new Random();
        int size = repos.size();
        return repos.get(random.nextInt(size));
    }
}
