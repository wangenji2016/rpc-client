package com.gupao.rpc;

import com.gupao.rpc.discovery.IServiceDiscovery;
import com.gupao.rpc.discovery.ServiceDiscovery;
import com.gupao.rpc.discovery.ZKConfig;

import java.util.HashMap;

public class ClientDemo {




    public static void main(String[] args) throws InterruptedException {
        IServiceDiscovery serviceDiscovery = new ServiceDiscovery(ZKConfig.CONNECT_STR);
        RpcClientProxy proxy = new RpcClientProxy(serviceDiscovery,null);

        for (int i =0; i<10; i++){
            IGpHello hello = proxy.clientProxy(IGpHello.class);
            System.out.println(hello.sayHello("wangenji"));
            Thread.sleep(1000);
        }



    }
}
